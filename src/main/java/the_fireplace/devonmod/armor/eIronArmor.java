package the_fireplace.devonmod.armor;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class eIronArmor extends ItemArmor{

	public eIronArmor(ArmorMaterial par2ArmorMaterial,
            int par3, int par4, String armornamePrefix)
{
super(par2ArmorMaterial, par3, par4);
this.material = par2ArmorMaterial;
par2ArmorMaterial.getDamageReductionAmount(par4);
this.setMaxDamage(par2ArmorMaterial.getDurability(par4));
this.maxStackSize = 1;
armorNamePrefix = armornamePrefix;
}
public String armorNamePrefix;
public ArmorMaterial material;
@Override
public boolean getIsRepairable(ItemStack tool, ItemStack material) {
	return material == new ItemStack(Items.emerald);
}
public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {

    if(stack.getItem() == DevonModBase.eIronHelmet || stack.getItem() == DevonModBase.eIronChestplate || stack.getItem() == DevonModBase.eIronBoots) {

            return "devonmod:textures/models/armor/e_iron_layer_1.png";

    }

    if(stack.getItem() == DevonModBase.eIronLeggings) {

            return "devonmod:textures/models/armor/e_iron_layer_2.png";

    }

    else return null;
}

}
