package the_fireplace.devonmod.armor;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class DirtArmor extends ItemArmor{

	public DirtArmor(ArmorMaterial p_i45325_1_, int p_i45325_2_, int p_i45325_3_) {
		super(p_i45325_1_, p_i45325_2_, p_i45325_3_);
	}
	//Texture Code
		public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
			if(stack.getItem() == DevonModBase.DirtHelmet || stack.getItem() == DevonModBase.DirtChestplate || stack.getItem() == DevonModBase.DirtBoots) {
				return "devonmod:textures/models/armor/dirt_layer_1.png"; 
			}
			if(stack.getItem() == DevonModBase.DirtLeggings) {
				return "devonmod:textures/models/armor/dirt_layer_2.png";
			}
			else {
				return null;
				}}
}
