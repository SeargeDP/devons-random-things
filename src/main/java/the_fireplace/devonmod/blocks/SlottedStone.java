package the_fireplace.devonmod.blocks;

import java.util.Random;

import the_fireplace.devonmod.DevonModBase;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class SlottedStone extends Block{

	public SlottedStone(Material par1Material) {
		super(par1Material);
		setHarvestLevel("pickaxe", 0);
		setBlockName("SlottedStone");
		setHardness(10F);
		setCreativeTab(DevonModBase.TabDevonMod);
	}
	//start texture code
	@SideOnly(Side.CLIENT)
	public static IIcon topIcon;
	@SideOnly(Side.CLIENT)
	public static IIcon bottomIcon;
	@SideOnly(Side.CLIENT)
	public static IIcon sideIcon;

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister icon) {
	topIcon = icon.registerIcon("devonmod:slottedstone_topbottom");
	bottomIcon = icon.registerIcon("devonmod:slottedstone_topbottom");
	sideIcon = icon.registerIcon("devonmod:slottedstone");
	}
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata) {
	if(side == 0) {
	return bottomIcon;
	} else if(side == 1) {
	return topIcon;
	} else {
	return sideIcon;
	}
	}
	//End texture code
    public boolean isLadder(World world, int x, int y, int z, EntityLivingBase entity)
    {
        return true;
    }
}
