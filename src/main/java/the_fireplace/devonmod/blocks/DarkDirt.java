package the_fireplace.devonmod.blocks;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

public class DarkDirt extends Block
{
    public DarkDirt(Material material)
    {
        super(material);
        setBlockTextureName("devonmod:DarkDirt");
        setHarvestLevel("shovel", 1);
        setBlockName("DarkDirt");
        setHardness(2F);
        setCreativeTab(DevonModBase.TabDevonMod);
    }
}
