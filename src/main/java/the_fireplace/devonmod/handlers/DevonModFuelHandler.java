package the_fireplace.devonmod.handlers;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.IFuelHandler;

public class DevonModFuelHandler implements IFuelHandler
{
    @Override
    public int getBurnTime(ItemStack fuel)
    {
        ItemStack var1 = fuel;

        if (var1 == new ItemStack(DevonModBase.ChargedCoalBlock))
        {
            return 102400;
        }
        else if (var1 == new ItemStack(DevonModBase.ChargedCoal))
        {
            return 12800;
        }
        else if (var1 == new ItemStack(DevonModBase.woodPaxel, 1, OreDictionary.WILDCARD_VALUE))
        {
            return 120;
        }
        else
        {
            return 0;
        }
    }
}