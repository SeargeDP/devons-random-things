package the_fireplace.devonmod.entities;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.monster.EntityBlaze;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityChargedCoal extends EntityThrowable implements IProjectile
{
    private static final double speed = 6.0;
    public EntityChargedCoal(World par1World)
    {
        super(par1World);
    }

    private int knockbackStrength;

    public EntityChargedCoal(World par1World, EntityLivingBase par2EntityLivingBase)
    {
        super(par1World, par2EntityLivingBase);
        this.motionX *= speed;
        this.motionY *= speed;
        this.motionZ *= speed;
    }

    public EntityChargedCoal(World par1World, double par2, double par4, double par6)
    {
        super(par1World, par2, par4, par6);
    }
    protected void onImpact(MovingObjectPosition movingobjectposition)
    {
        this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, 5.0F, true);
        this.setDead();
    }
    protected float getGravityVelocity()
    {
        return 0;
    }
    public void setKnockbackStrength(int par1)
    {
        this.knockbackStrength = par1;
    }
    public void setIsCritical(boolean par1)
    {
        byte b0 = this.dataWatcher.getWatchableObjectByte(16);

        if (par1)
        {
            this.dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 | 1)));
        }
        else
        {
            this.dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 & -2)));
        }
    }
    public boolean getIsCritical()
    {
        byte b0 = this.dataWatcher.getWatchableObjectByte(16);
        return (b0 & 1) != 0;
    }
}
