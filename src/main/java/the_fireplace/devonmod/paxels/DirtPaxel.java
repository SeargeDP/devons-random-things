package the_fireplace.devonmod.paxels;

import java.util.Set;

import the_fireplace.devonmod.DevonModBase;
import the_fireplace.fireplacecore.tools.ItemPaxel;

public class DirtPaxel extends ItemPaxel
{
    public DirtPaxel(ToolMaterial par2EnumToolMaterial, Set field_150914_c)
    {
        super(par2EnumToolMaterial, field_150914_c);
        setMaxStackSize(1);
        setCreativeTab(DevonModBase.TabDevonMod);
        setUnlocalizedName("DirtPaxel");
        setTextureName("devonmod:dirt_paxel");
    }
}
