package the_fireplace.devonmod.items;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.item.Item;

public class ChargedCoal extends Item{
	public ChargedCoal(){
		setTextureName("devonmod:chargedCoal");
		setCreativeTab(DevonModBase.TabDevonMod);
		setUnlocalizedName("ChargedCoal");
	}

}
