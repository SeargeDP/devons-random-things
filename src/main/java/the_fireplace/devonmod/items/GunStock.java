package the_fireplace.devonmod.items;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class GunStock extends Item
{
    public GunStock(){
        maxStackSize = 64;
        setCreativeTab(DevonModBase.TabDevonMod);
        setUnlocalizedName("GunStock");
        setTextureName("devonmod:stock");
    }
}