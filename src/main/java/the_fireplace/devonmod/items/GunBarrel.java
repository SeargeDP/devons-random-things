package the_fireplace.devonmod.items;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class GunBarrel extends Item
{
    public GunBarrel(){
        setCreativeTab(DevonModBase.TabDevonMod);
        setUnlocalizedName("GunBarrel");
        setTextureName("devonmod:barrel");
    }
}