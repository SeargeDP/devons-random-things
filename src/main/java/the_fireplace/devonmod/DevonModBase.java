package the_fireplace.devonmod;

import the_fireplace.devonmod.armor.*;
import the_fireplace.devonmod.blocks.*;
import the_fireplace.devonmod.handlers.DevonModFuelHandler;
import the_fireplace.devonmod.items.*;
import the_fireplace.devonmod.paxels.*;
import the_fireplace.devonmod.worldgen.WorldGeneratorDarkDirt;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import the_fireplace.fireplacecore.tools.ItemPaxel;

@Mod(modid="devonmod", name="Devon's Random Things", version="5.0.4", acceptedMinecraftVersions = "1.7.2,1.7.10", dependencies = "required-after:fireplacecore@[1.2.0,)")
public class DevonModBase {
	@Instance(value = "Devon's Random Things")
    public static DevonModBase instance;
	//Creative Tab
	public static CreativeTabs TabDevonMod = new DevonModCreativeTab(CreativeTabs.getNextID(), "Devon's Random Things");
	//Armor Materials
    public static ArmorMaterial DirtArmorMaterial = EnumHelper.addArmorMaterial("DIRT", 10, new int[]{1, 3, 2, 1}, 25);
    public static ArmorMaterial eArmorIron = EnumHelper.addArmorMaterial("eIRON", 15, new int[] {2, 6, 5, 2}, 19);
    public static ArmorMaterial eArmorGold = EnumHelper.addArmorMaterial("eGOLD", 7, new int[]{2, 5, 3, 1}, 35);
    public static ArmorMaterial eArmorDiamond = EnumHelper.addArmorMaterial("EDIAMOND", 33, new int[]{3, 8, 6, 3}, 20);
    //Tool Materials
    public static ToolMaterial toolDirt = EnumHelper.addToolMaterial("DIRT", 1, 20, 2.0F, 0.1F, 60);
	//Blocks
	public static Block ChargedCoalBlock = new ChargedCoalBlock(Material.rock);
	public static Block DarkDirt = new DarkDirt(Material.ground);
	public static Block SlottedStone = new SlottedStone(Material.rock);
	//Items
	public static Item ChargedCoal = new ChargedCoal();
	public static Item CoalGun = new CoalGun();
    public static Item GunBarrel = new GunBarrel();
    public static Item GunStock = new GunStock();
	//Armor
	public static Item DirtHelmet = new DirtArmor(DirtArmorMaterial, 1, 0).setCreativeTab(TabDevonMod).setUnlocalizedName("DirtHelmet").setTextureName("devonmod:DirtHelmet");
	public static Item DirtChestplate = new DirtArmor(DirtArmorMaterial, 1, 1).setCreativeTab(TabDevonMod).setUnlocalizedName("DirtChestplate").setTextureName("devonmod:DirtChestplate");
	public static Item DirtLeggings = new DirtArmor(DirtArmorMaterial, 1, 2).setCreativeTab(TabDevonMod).setUnlocalizedName("DirtLeggings").setTextureName("devonmod:DirtLeggings");
	public static Item DirtBoots = new DirtArmor(DirtArmorMaterial, 1, 3).setCreativeTab(TabDevonMod).setUnlocalizedName("DirtBoots").setTextureName("devonmod:DirtBoots");
    public static Item eIronHelmet = new eIronArmor(eArmorIron, 1, 0, "eIRON").setUnlocalizedName("eIronHelmet").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_iron_helmet");
    public static Item eIronChestplate = new eIronArmor(eArmorIron, 1, 1, "eIRON").setUnlocalizedName("eIronChestplate").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_iron_chestplate");
    public static Item eIronLeggings = new eIronArmor(eArmorIron, 1, 2, "eIRON").setUnlocalizedName("eIronLeggings").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_iron_leggings");
    public static Item eIronBoots = new eIronArmor(eArmorIron, 1, 3, "eIRON").setUnlocalizedName("eIronBoots").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_iron_boots");
    public static Item eGoldHelmet = new eGoldArmor(eArmorGold, 4, 0, "eGOLD").setUnlocalizedName("eGoldHelmet").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_gold_helmet");
    public static Item eGoldChestplate = new eGoldArmor(eArmorGold, 4, 1, "eGOLD").setUnlocalizedName("eGoldChestplate").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_gold_chestplate");
    public static Item eGoldLeggings = new eGoldArmor(eArmorGold, 4, 2, "eGOLD").setUnlocalizedName("eGoldLeggings").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_gold_leggings");
    public static Item eGoldBoots = new eGoldArmor(eArmorGold, 4, 3, "eGOLD").setUnlocalizedName("eGoldBoots").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_gold_boots");
    public static Item eDiamondHelmet = new eDiamondArmor(eArmorDiamond, 3, 0, "eDIAMOND").setUnlocalizedName("eDiamondHelmet").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_diamond_helmet");
    public static Item eDiamondChestplate = new eDiamondArmor(eArmorDiamond, 3, 1, "eDIAMOND").setUnlocalizedName("eDiamondChestplate").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_diamond_chestplate");
    public static Item eDiamondLeggings = new eDiamondArmor(eArmorDiamond, 3, 2, "eDIAMOND").setUnlocalizedName("eDiamondLeggings").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_diamond_leggings");
    public static Item eDiamondBoots = new eDiamondArmor(eArmorDiamond, 3, 3, "eDIAMOND").setUnlocalizedName("eDiamondBoots").setCreativeTab(TabDevonMod).setTextureName("devonmod:e_diamond_boots");
    //Equipment
    public static ItemPaxel dirtPaxel = new DirtPaxel(toolDirt, ItemPaxel.breakSet);
    public static ItemPaxel woodPaxel = new WoodPaxel(ToolMaterial.WOOD, ItemPaxel.breakSet);
    public static ItemPaxel stonePaxel = new StonePaxel(ToolMaterial.STONE, ItemPaxel.breakSet);
    public static ItemPaxel ironPaxel = new IronPaxel(ToolMaterial.IRON, ItemPaxel.breakSet);
    public static ItemPaxel goldPaxel = new GoldPaxel(ToolMaterial.GOLD, ItemPaxel.breakSet);
    public static ItemPaxel diamondPaxel = new DiamondPaxel(ToolMaterial.EMERALD, ItemPaxel.breakSet);
	@EventHandler
    public void preInit(FMLPreInitializationEvent event) {
		//Block Registry
		GameRegistry.registerBlock(ChargedCoalBlock, "ChargedCoalBlock");
        GameRegistry.registerBlock(DarkDirt, "DarkDirt");
        GameRegistry.registerBlock(SlottedStone, "SlottedStone");
		//Item Registry
		GameRegistry.registerItem(ChargedCoal, "ChargedCoal");
		GameRegistry.registerItem(CoalGun, "CoalGun");
        GameRegistry.registerItem(GunBarrel, "DevonModGunBarrel");
        GameRegistry.registerItem(GunStock, "DevonModGunStock");
		//Armor Registry
		GameRegistry.registerItem(DirtHelmet, "DirtHelmet");
		GameRegistry.registerItem(DirtChestplate, "DirtChestplate");
		GameRegistry.registerItem(DirtLeggings, "DirtLeggings");
		GameRegistry.registerItem(DirtBoots, "DirtBoots");
        GameRegistry.registerItem(eIronHelmet, "eIronHelmet");
        GameRegistry.registerItem(eIronChestplate, "eIronChestplate");
        GameRegistry.registerItem(eIronLeggings, "eIronLeggings");
        GameRegistry.registerItem(eIronBoots, "eIronBoots");
        GameRegistry.registerItem(eGoldHelmet, "eGoldHelmet");
        GameRegistry.registerItem(eGoldChestplate, "eGoldChestplate");
        GameRegistry.registerItem(eGoldLeggings, "eGoldLeggings");
        GameRegistry.registerItem(eGoldBoots, "eGoldBoots");
        GameRegistry.registerItem(eDiamondHelmet, "eDiamondHelmet");
        GameRegistry.registerItem(eDiamondChestplate, "eDiamondChestplate");
        GameRegistry.registerItem(eDiamondLeggings, "eDiamondLeggings");
        GameRegistry.registerItem(eDiamondBoots, "eDiamondBoots");
        //Paxel Registry
        GameRegistry.registerItem(dirtPaxel, "dirtPaxel");
        GameRegistry.registerItem(woodPaxel, "woodPaxel");
        GameRegistry.registerItem(stonePaxel, "stonePaxel");
        GameRegistry.registerItem(ironPaxel, "ironPaxel");
        GameRegistry.registerItem(goldPaxel, "goldPaxel");
        GameRegistry.registerItem(diamondPaxel, "diamondPaxel");
		//Block ItemStacks
		ItemStack ChargedCoalBlockStack = new ItemStack(ChargedCoalBlock);
		ItemStack ChargedCoalBlockStack8 = new ItemStack(ChargedCoalBlock, 8);
		ItemStack CoalBlockStack = new ItemStack(Blocks.coal_block);
		ItemStack RedstoneBlockStack = new ItemStack(Blocks.redstone_block);
      	ItemStack CoalBlockStack9 = new ItemStack(Blocks.coal_block, 9);
        ItemStack DirtStack = new ItemStack(Blocks.dirt);
    	ItemStack DirtStack9 = new ItemStack(Blocks.dirt, 9);
        ItemStack DarkDirtStack = new ItemStack(DarkDirt);
        ItemStack SlottedStoneStack4 = new ItemStack(SlottedStone, 4);
        ItemStack StoneSlabStack = new ItemStack(Blocks.stone_slab);
        ItemStack WoodStack = new ItemStack(Blocks.planks);
        ItemStack HayStack = new ItemStack(Blocks.hay_block);
        ItemStack TntStack = new ItemStack(Blocks.tnt);
        ItemStack NetherBrickStack = new ItemStack(Blocks.nether_brick);
        ItemStack BricksStack = new ItemStack(Blocks.brick_block);
        ItemStack QuartzBlockStack = new ItemStack(Blocks.quartz_block);
        ItemStack IronBlockStack = new ItemStack(Blocks.iron_block);
        ItemStack EndStoneStack = new ItemStack(Blocks.end_stone);
        ItemStack SoulSandStack = new ItemStack(Blocks.soul_sand);
        ItemStack IronOreStack = new ItemStack(Blocks.iron_ore);
        ItemStack NetherQuartzOreStack = new ItemStack(Blocks.quartz_ore);
        ItemStack FenceStack = new ItemStack(Blocks.fence);
        ItemStack nbFenceStack = new ItemStack(Blocks.nether_brick_fence);
        ItemStack BrickStairsStack = new ItemStack(Blocks.brick_stairs);
        ItemStack nbStairsStack = new ItemStack(Blocks.nether_brick_stairs);
        ItemStack ObsidianStack = new ItemStack(Blocks.obsidian);
        ItemStack NetherrackStack = new ItemStack(Blocks.netherrack);
        ItemStack GlowstoneStack = new ItemStack(Blocks.glowstone);
        ItemStack CobblestoneStack = new ItemStack(Blocks.cobblestone);
		//Item ItemStacks
		ItemStack ChargedCoalStack = new ItemStack(ChargedCoal);
		ItemStack ChargedCoalStack8 = new ItemStack(ChargedCoal, 8);
		ItemStack CoalStack = new ItemStack(Items.coal, 1, OreDictionary.WILDCARD_VALUE);
		ItemStack RedstoneStack = new ItemStack(Items.redstone);
        ItemStack IronStack = new ItemStack(Items.iron_ingot);
        ItemStack EmeraldStack = new ItemStack(Items.emerald);
        ItemStack FlintStack = new ItemStack(Items.flint);
        ItemStack StickStack = new ItemStack(Items.stick);
        ItemStack DiamondStack = new ItemStack(Items.diamond);
        ItemStack ReedStack = new ItemStack(Items.reeds);
        ItemStack StringStack = new ItemStack(Items.string);
        ItemStack GoldStack = new ItemStack(Items.gold_ingot);
        ItemStack StringStack4 = new ItemStack(Items.string, 4);
        ItemStack EnderPearlStack = new ItemStack(Items.ender_pearl);
        ItemStack LavaStack = new ItemStack(Items.lava_bucket);
        ItemStack BlazePowderStack = new ItemStack(Items.blaze_powder);
        ItemStack OrangeDyeStack = new ItemStack(Items.dye, 1, 14);
        ItemStack GlowstoneDustStack = new ItemStack(Items.glowstone_dust);
        ItemStack MagmaCreamStack = new ItemStack(Items.magma_cream);
        ItemStack BlazeRodStack = new ItemStack(Items.blaze_rod);
        ItemStack NetherrackBrickStack = new ItemStack(Items.netherbrick);
        ItemStack BrickStack = new ItemStack(Items.brick);
        ItemStack NetherQuartzStack = new ItemStack(Items.quartz);
        ItemStack BottleStack = new ItemStack(Items.glass_bottle);
        ItemStack NetherWartStack = new ItemStack(Items.nether_wart);
        ItemStack WheatStack = new ItemStack(Items.wheat);
		//Tools, Weapons, and Parts
        ItemStack GunBarrelStack = new ItemStack(GunBarrel);
        ItemStack GunStockStack = new ItemStack(GunStock);
        ItemStack FlintAndSteelStack = new ItemStack(Items.flint_and_steel);
        ItemStack CoalGunStack = new ItemStack(CoalGun);
        ItemStack DirtHelmStack = new ItemStack(DirtHelmet);
        ItemStack DirtChestStack = new ItemStack(DirtChestplate);
        ItemStack DirtLegStack = new ItemStack(DirtLeggings);
        ItemStack DirtBootsStack = new ItemStack(DirtBoots);
        ItemStack DirtPaxelStack = new ItemStack(dirtPaxel);
        ItemStack WoodPaxelStack = new ItemStack(woodPaxel);
        ItemStack StonePaxelStack = new ItemStack(stonePaxel);
        ItemStack IronPaxelStack = new ItemStack(ironPaxel);
        ItemStack GoldPaxelStack = new ItemStack(goldPaxel);
        ItemStack DiamondPaxelStack = new ItemStack(diamondPaxel);
        ItemStack eIronHelmStack = new ItemStack(eIronHelmet);
        ItemStack eIronChestStack = new ItemStack(eIronChestplate);
        ItemStack eIronLegsStack = new ItemStack(eIronLeggings);
        ItemStack eIronBootsStack = new ItemStack(eIronBoots);
        ItemStack eGoldHelmStack = new ItemStack(eGoldHelmet);
        ItemStack eGoldChestStack = new ItemStack(eGoldChestplate);
        ItemStack eGoldLegsStack = new ItemStack(eGoldLeggings);
        ItemStack eGoldBootsStack = new ItemStack(eGoldBoots);
        ItemStack eDiamondHelmStack = new ItemStack(eDiamondHelmet);
        ItemStack eDiamondChestStack = new ItemStack(eDiamondChestplate);
        ItemStack eDiamondLegsStack = new ItemStack(eDiamondLeggings);
        ItemStack eDiamondBootsStack = new ItemStack(eDiamondBoots);
		//Recipes
		GameRegistry.addRecipe(ChargedCoalBlockStack, "xxx", "xxx", "xxx",
				'x', ChargedCoalStack);
		GameRegistry.addRecipe(ChargedCoalBlockStack8, "xxx", "xyx", "xxx",
				'x', CoalBlockStack, 'y', RedstoneBlockStack);
		GameRegistry.addRecipe(ChargedCoalStack8, "xxx", "xyx", "xxx",
				'x', CoalStack, 'y', RedstoneStack);
        GameRegistry.addRecipe(GunBarrelStack, "xxx", "  y", "xxx",
                'x', IronStack, 'y', FlintAndSteelStack);
        GameRegistry.addRecipe(GunStockStack, "xyx", "yxy",
			'x', IronStack, 'y', FlintStack);
		GameRegistry.addRecipe(CoalGunStack, "yx",
			'x', GunStockStack, 'y', GunBarrelStack);
		GameRegistry.addRecipe(DirtHelmStack, "xxx", "x x",
			'x', DarkDirtStack);
		GameRegistry.addRecipe(DirtChestStack, "x x", "xxx", "xxx",
			'x', DarkDirtStack);
		GameRegistry.addRecipe(DirtLegStack, "xxx", "x x", "x x",
			'x', DarkDirtStack);
		GameRegistry.addRecipe(DirtBootsStack, "x x", "x x",
			'x', DarkDirtStack);
		GameRegistry.addRecipe(DirtPaxelStack, "xxx", "xy ", " y ",
			'x', DarkDirtStack, 'y', StickStack);
		GameRegistry.addRecipe(DirtPaxelStack, "xxx", " yx", " y ",
			'x', DarkDirtStack, 'y', StickStack);
		GameRegistry.addRecipe(WoodPaxelStack, "xxx", "xy ", " y ",
			'x', WoodStack, 'y', StickStack);
		GameRegistry.addRecipe(WoodPaxelStack, "xxx", " yx", " y ",
			'x', WoodStack, 'y', StickStack);
		GameRegistry.addRecipe(StonePaxelStack, "xxx", "xy ", " y ",
			'x', CobblestoneStack, 'y', StickStack);
		GameRegistry.addRecipe(StonePaxelStack, "xxx", " yx", " y ",
			'x', CobblestoneStack, 'y', StickStack);
		GameRegistry.addRecipe(IronPaxelStack, "xxx", "xy ", " y ",
			'x', IronStack, 'y', StickStack);
		GameRegistry.addRecipe(IronPaxelStack, "xxx", " yx", " y ",
			'x', IronStack, 'y', StickStack);
		GameRegistry.addRecipe(GoldPaxelStack, "xxx", "xy ", " y ",
			'x', GoldStack, 'y', StickStack);
		GameRegistry.addRecipe(GoldPaxelStack, "xxx", " yx", " y ",
			'x', GoldStack, 'y', StickStack);
		GameRegistry.addRecipe(DiamondPaxelStack, "xxx", "xy ", " y ",
			'x', DiamondStack, 'y', StickStack);
		GameRegistry.addRecipe(DiamondPaxelStack, "xxx", " yx", " y ",
			'x', DiamondStack, 'y', StickStack);
		GameRegistry.addRecipe(DarkDirtStack, "xxx", "xxx", "xxx",
			'x', DirtStack);
		GameRegistry.addRecipe(SlottedStoneStack4, "xxx", "xxx", "xxx",
			'x', StoneSlabStack);
		GameRegistry.addRecipe(eIronHelmStack, "xyx", "x x",
			'x', IronStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eIronLegsStack, "xxx", "y y", "x x",
			'x', IronStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eIronChestStack, "x x", "xyx", "xxx",
			'x', IronStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eIronBootsStack, "x x", "y y",
			'x', IronStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eGoldHelmStack, "xyx", "x x",
			'x', GoldStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eGoldLegsStack, "xxx", "y y", "x x",
			'x', GoldStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eGoldChestStack, "x x", "xyx", "xxx",
			'x', GoldStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eGoldBootsStack, "x x", "y y",
			'x', GoldStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eDiamondHelmStack, "xyx", "x x",
			'x', DiamondStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eDiamondLegsStack, "xxx", "y y", "x x",
			'x', DiamondStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eDiamondChestStack, "x x", "xyx", "xxx",
			'x', DiamondStack, 'y', EmeraldStack);
		GameRegistry.addRecipe(eDiamondBootsStack, "x x", "y y",
					'x', DiamondStack, 'y', EmeraldStack);
    	//Ore Gen Registry
		GameRegistry.registerWorldGenerator(new WorldGeneratorDarkDirt(), 12);
		//Fuel Handler
        GameRegistry.registerFuelHandler(new DevonModFuelHandler());
    }
}
