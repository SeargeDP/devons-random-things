package the_fireplace.devonmod;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class DevonModCreativeTab extends CreativeTabs{

	public DevonModCreativeTab(int par1, String par2Str) {
		super(par1, par2Str);
	}

	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(DevonModBase.ChargedCoalBlock);
	}
	@Override
    @SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel(){
		return "Devon's Random Things";
	}

}
